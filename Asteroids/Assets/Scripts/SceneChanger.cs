﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour
{

    public Animator animator;

    private int levelToLoad;

    public void FadeToNextLevel()
    {
        FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void FadeToLevel(int levelIndex)
    {
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");
        OnFadeComplete();
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log(scene.name);
        if (scene.name == "EndGame")
        {
            // Get the final score and destroy the GameController.
            GameObject go;
            GameController gc;

            go = GameObject.Find("GameController");
            gc = go.GetComponent<GameController>();
            GameObject.Find("Score").GetComponent<Text>().text = " FINAL SCORE: " + '\n' + gc.score.ToString("D13");

            Debug.Log(">>> " + gc.score);

            Destroy(go);
        }
    }
}
