﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MenuButtons : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Button btn;
    private Text txt;
    private string originalTxt;

    public void Start()
    {
        btn = this.GetComponent<Button>();
        txt = GetComponentInChildren<Text>();
        originalTxt = txt.text;
    }

    public void Update()
    {

    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        txt.text = "> " + originalTxt;
        txt.color = Color.red;
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        txt.text = originalTxt;
        txt.color = Color.white;
    }
}