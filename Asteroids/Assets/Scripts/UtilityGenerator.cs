﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilityGenerator : MonoBehaviour
{
    public List<GameObject> UtilityModels;
    public float minSpawnTime = 5;
    public float maxSpawnTime = 10;
    public float SpawnBoxWidth = 100;
    public float SpawnBoxHeight = 100;
    public float SpawnBoxDepth = 400;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("CreateUtilities", 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateUtilities()
    {
        if (UtilityModels.Count != 0)
        {
            GameObject a = Instantiate(UtilityModels[Random.Range(0,UtilityModels.Count)], new Vector3(Random.Range(-SpawnBoxWidth, SpawnBoxWidth), Random.Range(-SpawnBoxHeight, SpawnBoxHeight), SpawnBoxDepth), Quaternion.identity);
            // UTILITY GENERATOR ATTACHES:
            // 1. MoveTowardsCamera Script
            a.AddComponent<MoveTowardsCamera>();
        }

        Invoke("CreateUtilities", Random.Range(minSpawnTime, maxSpawnTime));
    }
}
