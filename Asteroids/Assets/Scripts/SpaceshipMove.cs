﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpaceshipMove : MonoBehaviour
{
    public float speed = 15f;
    public float rotationSpeed = 0.04f;
    public float maxRotation = 0.30f;
    Quaternion initRotationShip;
    bool restoreRotation = false;

    // Start is called before the first frame update
    void Start()
    {
        initRotationShip = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(transform.rotation.y.ToString("0.00"));
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(speed * GameController.gc.spaceshipSpeedModifier * Time.deltaTime, 0, 0);
            if (Mathf.Abs(transform.rotation.z) < maxRotation)
                transform.Rotate(0, 0, -1, Space.Self);
            restoreRotation = true;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-speed * GameController.gc.spaceshipSpeedModifier * Time.deltaTime, 0, 0);
            if (Mathf.Abs(transform.rotation.z) < maxRotation)
                transform.Rotate(0, 0, 1, Space.Self);
            restoreRotation = true;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += new Vector3(0, speed * GameController.gc.spaceshipSpeedModifier * Time.deltaTime, 0);
            if (Mathf.Abs(transform.rotation.x) < maxRotation)
                transform.Rotate(-1, 0, 0, Space.Self);
            restoreRotation = true;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += new Vector3(0, -speed * GameController.gc.spaceshipSpeedModifier * Time.deltaTime, 0);
            if (Mathf.Abs(transform.rotation.x) < maxRotation)
                transform.Rotate(1, 0, 0, Space.Self);
            restoreRotation = true;
        }

        if (restoreRotation)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, initRotationShip, rotationSpeed);

            if (initRotationShip == transform.rotation)
                restoreRotation = false;
        }
        

        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //    transform.position += new Vector3(0, 0, -speed * Time.deltaTime);
        //}
    }

    private void LateUpdate()
    {

    }
}
