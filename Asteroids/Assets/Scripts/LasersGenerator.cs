﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LasersGenerator : MonoBehaviour
{
    public GameObject Bullet_Emitter;
    public GameObject Bullet;
    public float LaserTravelTime;
    public float LaserPower = 100000000;
    private ArrayList AllLasers;

    private void Start()
    {
        AllLasers = new ArrayList();
    }

    // Update is called once per frame
    void Update()
    {
        GameObject Temporary_Bullet_Handler = null;

        if (Input.GetKeyDown("space"))
        {
            Temporary_Bullet_Handler = Instantiate(Bullet, Bullet_Emitter.transform.position + new Vector3(0,0,10), Quaternion.identity) as GameObject;
            Rigidbody Temporary_RigidBody;
            Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody>();
            Temporary_RigidBody.AddForce(Temporary_RigidBody.transform.forward * LaserPower, ForceMode.Impulse);
            Destroy(Temporary_Bullet_Handler, 5.0f);
        }

    }

}
