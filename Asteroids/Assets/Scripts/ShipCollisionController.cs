﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCollisionController : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        string name = collision.gameObject.tag;
        GameController g = GameController.gc;
        
        // Are we colliding with an Asteroid or PowerUps? 
        if (name == "Asteroid")
        {
            if (g.godmode)
            {
                g.AdjustScore(10);
            }
            else
            {
                g.AdjustLife(-1);
            }
        } else
        {
            if (name == "Bender")
                g.AdjustScore(100);
            else
            {
                g.addUtility(name);
            }
            Destroy(collision.gameObject);
        }
    }
}
