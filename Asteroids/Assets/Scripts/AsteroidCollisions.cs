﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AsteroidCollisions : MonoBehaviour
{

    public Canvas overlay;
    public int hitBonus = 10;
    public GameObject ShatterModel;
    public float ShatterZCorrection;
    private int score = 0;
    private int life = 3;
    private Text scoreTxt;
    private Text lifeTxt;
    private List<ParticleCollisionEvent> collisionEvents;
    private ParticleSystem ps;

    // Start is called before the first frame update
    void Start()
    {
        Text[] canvas = overlay.GetComponentsInChildren<Text>();
        foreach(Text t in canvas)
        {
            if (t.name == "Score")
                scoreTxt = t;
            else if (t.name == "Life")
                lifeTxt = t;
        }
        collisionEvents = new List<ParticleCollisionEvent>();
        ps = this.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnParticleCollision(GameObject other)
    {
        // TEST
        collisionEvents = new List<ParticleCollisionEvent>();
        ps = this.GetComponent<ParticleSystem>();

        ps.GetCollisionEvents(other, collisionEvents);

        GameObject Shatter = Instantiate(ShatterModel, collisionEvents[0].colliderComponent.transform.position, Quaternion.identity);
        //Shatter.transform.position = collisionEvents[0].intersection;
        Shatter.transform.localScale = new Vector3(3, 3, 3);
        Destroy(Shatter, 3.0f);
        //Debug.Log("SHIP : " + other.transform.position);
        //Debug.Log("COLL : " + collisionEvents[0].colliderComponent.transform.position);
        //Debug.Log("INTER : " + collisionEvents[0].intersection);
        
        // TEST

        if (other.name == "Laser(Clone)") {
            score += hitBonus;
            scoreTxt.text = "SCORE : " + score.ToString("D8");           
        }

        if (other.name == "Spaceship") {
            life--;
            string s = "";

            for (int i= 0; i < life; i++)
            {
                s += "å";
            }
            lifeTxt.text = "LIFE : " + s.PadLeft(8);
        }
    }
}
