﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCollisions : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        string s = collision.gameObject.tag;
        GameController g = GameController.gc;

        // Score adjustments.
        if (s == "Bender")
            g.AdjustScore(-100);
        else if (s == "Asteroid")
            g.AdjustScore(10);

        Destroy(gameObject);
    }

}
