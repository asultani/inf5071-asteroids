﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShatterOnCollision : MonoBehaviour
{
    // ATTACHED TO THE SHATTER MODEL.
    public float radius = 5f;
    public float power = 700.0F;

    void Start()
    {
        if (tag == "Asteroid")
            power = power * GameController.gc.asteroidSpeedModifier;

        Destroy(gameObject, 3.0f);
        Vector3 explosionPos = transform.position;
        Rigidbody[] frags = gameObject.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in frags)
        {
            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 0.0F);
        }

    }

}
