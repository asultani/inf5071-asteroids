﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform spaceship;
    public int yOffset = 9;
    public int zOffset = 10;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(spaceship.position.x, spaceship.position.y + yOffset, spaceship.position.z + zOffset);
    }
}
