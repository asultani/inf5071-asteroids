﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;

public class GameController : MonoBehaviour
{
    // Static controller
    public static GameController gc;

    // Linked Objects
    public Text UIScore;
    public Text UILife;
    public Text UISpeed;
    public GameObject PrefabForceField;
    public SceneChanger levelChanger;

    // Game Variables
    public int life = 3;
    public int score = 0;
    public float spaceshipSpeedModifier = 1;
    public int novaBombs = 0;
    public bool godmode = false;
    public int currentWeapon = 1;
    public float asteroidSpeedModifier = 5;

    // Private Game Variables
    private GameObject ForceField;
    private float elapsed = 0;


    private void Awake()
    {
        if (gc == null)
        {
            DontDestroyOnLoad(gameObject);
            gc = this;
        }
        else if (gc != this)
            Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        // Create the force field.
        ForceField = Instantiate(PrefabForceField, GameObject.FindGameObjectWithTag("Spaceship").transform);
        ForceField.SetActive(false);

        // Set init speed
        UISpeed.text = "SPEED :\n" + spaceshipSpeedModifier.ToString("P");

        // Set init life
        AdjustLife(0);
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= 1)
        {
            elapsed = elapsed % 1;
            AdjustScore(1);
        }
    }

    public void AdjustLife(int v)
    {
        if (godmode)
            return;

        if (life + v <= 0)
            levelChanger.FadeToNextLevel();

        life += v;
        string s = "";

        for (int i = 0; i < life; i++)
        {
            s += "å";
        }
        UILife.text = "HITPOINTS :\n" + s;
    }

    public void AdjustScore(int v)
    {
        score += v;
        UIScore.text = "SCORE : \n" + score.ToString("D10");
    }

    public void addUtility(string p)
    {
        switch (p)
        {
            case "Life":
                if (life < 10)
                    AdjustLife(1);
                break;
            case "Star":
                godmode = true;
                ForceField.SetActive(true);
                CancelInvoke("removeGodmode");
                Invoke("removeGodmode", 60);
                break;
            case "Meteor":
                // Speed.
                spaceshipSpeedModifier += 0.1f;
                UISpeed.text = "SPEED :\n" + spaceshipSpeedModifier.ToString("P");
                Invoke("removeSpeed", 60);
                break;
            case "Nova":
                novaBombs++;
                break;
            case "Weapon":
                currentWeapon = 2;
                break;
        }
    }

    // Utility removals.
    void removeSpeed()
    {
            spaceshipSpeedModifier -= 0.1f;
            UISpeed.text = "SPEED :\n" + spaceshipSpeedModifier.ToString("P");
    }

    void removeGodmode()
    {
        godmode = false;
        ForceField.SetActive(false);
    }
}
