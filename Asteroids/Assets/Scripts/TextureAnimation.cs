﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureAnimation : MonoBehaviour
{
    public float scrollSpeed = 0.5f;

    private Material m;

    // Start is called before the first frame update
    void Start()
    {
        m = gameObject.GetComponentInChildren<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        // Animating the textures.
        m.SetTextureOffset("_MainTex", new Vector2(Time.time * scrollSpeed, Time.time * scrollSpeed));
        m.SetTextureOffset("_DetailAlbedoMap", new Vector2(Time.time * scrollSpeed, Time.time * scrollSpeed));
    }
}
