﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour
{
    public GameObject Asteroids;
    public float CreationSpeed = 0.1f;
    public float SpawnBoxWidth = 100;
    public float SpawnBoxHeight = 100;
    public float SpawnBoxDepth = 400;
    public float minAsteroidScale = 1;
    public float maxAsteroidScale = 3;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateAsteroids", 0, CreationSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        

    }

    void CreateAsteroids()
    {
        GameObject a = Instantiate(Asteroids, new Vector3(Random.Range(-SpawnBoxWidth, SpawnBoxWidth), Random.Range(-SpawnBoxHeight, SpawnBoxHeight), SpawnBoxDepth), Random.rotation);
        float randomScale = Random.Range(minAsteroidScale, maxAsteroidScale);
        a.transform.localScale = new Vector3(randomScale, randomScale, randomScale);
    }
}
