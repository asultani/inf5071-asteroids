﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsCamera : MonoBehaviour
{
    public int zDestroyLimit = -30;
    public int speed = 12;
    public bool randomRotate = true;
    public float rotationFactor = 1.5f;
    private float[] rotationFactors;


    private void Start()
    {
        rotationFactors = new float[3] { Random.Range(-1, 1) * rotationFactor, Random.Range(-1, 1) * rotationFactor, Random.Range(-1, 1) * rotationFactor };
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position + new Vector3(0, 0, -1) * speed * Time.deltaTime;
        if (transform.position.z < zDestroyLimit)
        {
            Destroy(gameObject);
        }

        if (randomRotate)
        {
            transform.Rotate(rotationFactors[0], rotationFactors[1], rotationFactors[2]);
        }
    }
}
