﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    // ATTACHED TO THE HIGH-POLY MODELS.
    public GameObject ShatterAsteroid;
    public int zDestroyLimit = -30;
    public int speed = 25;
    private float[] rotationFactors;

    private void Start()
    {
        rotationFactors = new float[3];
        rotationFactors[0] = Random.Range(-1, 1);
        rotationFactors[1] = Random.Range(-1, 1);
        rotationFactors[2] = Random.Range(-1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position + new Vector3(0,0,-1) * speed * GameController.gc.asteroidSpeedModifier * Time.deltaTime;
        transform.Rotate(rotationFactors[0], rotationFactors[1], rotationFactors[2]);
        if (transform.position.z < zDestroyLimit)
        {
            Destroy(gameObject);
        }


    }

    private void OnCollisionEnter(Collision other)
    {
        // Replace existing asteroid with the shattered version.
        GameObject a = Instantiate(ShatterAsteroid, gameObject.transform.position, gameObject.transform.rotation);
        a.transform.localScale = gameObject.transform.localScale;
        Destroy(gameObject, 0);
    }
}
