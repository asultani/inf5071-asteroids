﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BenderCollisionController : MonoBehaviour
{
    public GameObject ShatterModel;

    private void OnCollisionEnter(Collision collision)
    {
        string name = collision.collider.tag;
        if (name == "Laser")
        {
            GameObject a = Instantiate(ShatterModel, gameObject.transform.position, gameObject.transform.rotation);
            GameController.gc.AdjustScore(-100);
            Destroy(gameObject);
        }
    }
}
