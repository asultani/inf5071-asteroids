﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BenderCross : MonoBehaviour
{
    private Vector3 startPos;
    public double MaxPos;
    public float TransSpeed = 1;
    public float RotSpeedX = 1;
    public float RotSpeedY = 1;
    public float RotSpeedZ = 1;
    public float AnimSpeed = 1;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;

        animator = this.GetComponentInChildren<Animator>();

    }

    private void OnGUI()
    {
        animator.speed = AnimSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * TransSpeed, Space.World);
        transform.Rotate(RotSpeedX, RotSpeedY, RotSpeedZ, Space.World);
        if (transform.position.x > MaxPos)
            transform.position = startPos;

    }
}
