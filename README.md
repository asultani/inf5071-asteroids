# Asteroids

![MainMenu](./img/MainMenu.png)

## Project description

The purpose of the project was to create a 3D Game from scratch. All the assets (animations, textures, models) in this game have been created by myself through the use of various programs :

- [SpaceScape](http://alexcpeterson.com/spacescape/)
- [Photoshop](https://www.adobe.com/ca/products/photoshop.html)
- [ImageMagick](https://imagemagick.org/index.php)
- [Blender](https://www.blender.org/)

The game engine used is [Unity v2018.3.6f1](https://unity3d.com/).

This project has been completed as part of the infographic class (*INF5071*) of winter 2019 taught by professor Alexandre Blondin-Massé (@ablondin).

## Author

Ali Reza Sultani

## Concept

The original concept stems from one of the first major hits of the golden age of arcade games. [Asteroids](<https://en.wikipedia.org/wiki/Asteroids_(video_game)>) is an arcade style single player shooter game created by Atari back in 1979.

The concept behind the game is quite simple. The player controls a spaceship that he can move, rotate and shoot in order to destroy asteroids and enemy saucers for points. As the player's score increases, the number of asteroids and the accuracy of the saucers increases and thus the game becomes gradually more and more difficult. Each player starts with 3-5 lives and can gain an extra life every 10,000 points. The objective of the game is to gain as many points as possible.

| ![Asteroids](./img/asteroidsGIF.gif) |
|:--:|
|*Asteroids*|

However, the visual perspective will be different in this version. The game will present a view similar to the classic NES side-scrolling shooter games such as in *Crisis Force*, *1943* and *The Guardian Legend*. The original Asteroids presented an overhead view and allowed the player to move his ship outside the boundary of the screen in order to loop back on the opposite side. Such behavior is usually not present within side-scrollers.

| ![The Guardian Legend](./img/guardianlegend.gif) |
|:--:|
| *The Guardian Legend* |

I have put in considerable amount of time playing some of these NES games during my childhood. These games are without a doubt an influence in the conception of this game. 

## Gameplay

The main concept is going to be essentially a remake of the Asteroids game, with some of the gameplay aspects of the above classic NES games. The game mechanics are quite intuitive and self-explanatory. As previously stated, it is essentially a remake of the Asteroids game. 

![GamePlay](./img/GamePlay.gif)

The game will enable the player to fully experience the 3D aspect of a space shooter game by giving the player a depth view of the asteroids field rather than the horizontal/vertical view that are typically available in side scrollers. You can gain points by surviving, or shooting down asteroids. (See [Concept](#concept))

The game also incorporates different power ups, however, it's implementation is incomplete at this stage and more details will be given once completed.

## Controls

Use the **keyboard arrows** in order to move the spaceship in a given direction and **spacebar** to fire.

## Supported Platform

The game has been developed and tested on a *Windows 10 x64* machine.

## Dependencies

There is currently no dependencies needed to be able to run the game.

## Installation/Execution

There is currently two ways to play the game :

 	1. Run the pre-compiled executable for the game in the `Builds/Windows/x86` folder.
 	2. Build your own version using Unity.

Currently the pre-compiled build is only valid for the `Windows x86` version, if you want to compile
a version for your machine, you can do so through the Unity Editor.

 	1. Click `File > Build Settings`.
 	2. Select your platform from the menu on the left.
 	3. Press `Build` or `Build and Run`

It is recommended to use **Unity version 2018.3.6f1** which is the same version as the one I
have used to develop this project. Unity could have issues when you try to build the game with 
a different version of Unity.

## Known Issues

**Why does the game animation suddenly freeze on the main menu?**

The pre-compiled build might freeze up on the main menu depending on your computer. I have tried it on two different computers, and one of them seem to experience this issue. But no worries, you can still play!
Even though it looks like the main menu is glitched out, just press enter or click play.

I am still investigating as to why this happens on one computer and not another.

**Some of the game features don't seem to be working! What happened to the power ups?**

I have been unable to implement the code behind <u>ALL</u> the power ups given how much time I had. Keep in mind I developed this game on my own while holding a full course load. However, I do intend to complete this game on my own spare time this summer since I had very much enjoyed making it!

## Tasks

The project is currently divided in 4 tasks :

- [X] TASK 1: Adding the main menu.
- [X] TASK 2: Adding the game framework.
	- [X] TASK 2.1: Modeling the asteroids and main ship.
	- [X] TASK 2.2: Object animation and interactions.
    - [X] TASK 2.2: Gameplay.
- [X] TASK 3: Adding the credits screen.
- [X] TASK 4: Documentation and refactoring.



## Content

The project has three main folders:

- `/Presentations/`
  - Contains the slides of the two *PowerPoint* presentations for the game. (French)
- `/Builds/`
  - Contains the pre-compiled executables for the game.
- `/Asteroids/`
  - This folder contains the source file of the project, as well as the various *Unity* editor files.
  - `/Asteroids/Assets/` contains all the textures/models/armatures/etc... used in the making of this game. The folder names are self-explanatory as to what they contain, they are divided by component type/game object. Please note most of these folders will also contain a folder for the `../Textures/` and `../Materials/` inside.

## License

The project uses the **GNU General Public License v3**. Please consult the license file in the root folder of this repository for more information.

## References

All the assets, except the *Arcade* font referenced below, were created by myself. You will also find below a short list of some of the useful tutorials that I have been following in order to use *Blender* and *Photoshop* properly.

**Font:**

- Yuji Adachi - Typography (*Arcade* Font)
  - www.9031.com

**Tutorials :**

- Pavel Křupala (Blender Freak)
  - www.blenderfreak.com
- Andrew Price (Blender Guru)
  - www.blenderguru.com
- RiverCityGraphix - Photoshop tutorials
  - www.youtube.com/RiverCityGraphix

## Status

Game playable and complete.
The only thing missing is the implementation of some of the power ups.