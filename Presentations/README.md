Given the immense size of these slides (due to the heavy use of images and GIFs). I would highly recommend accessing the slides through *Google Slides* directly :

[The first presentation.](https://docs.google.com/presentation/d/19PDCjLaZSsazkEvZJTN7PmsvK3AriACpYVvdASBvyzQ/edit?usp=sharing)

[The second presentation.](https://docs.google.com/presentation/d/1j2jRH5-LTdyeFjlopKla92gMS5T9QJZn1ethDoP62z0/edit?usp=sharing)